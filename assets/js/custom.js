jQuery(document).ready(function($) {

  var playlistId = 'PLcwfTujo03do3X7VGxO1xTvjAfj4r3CzZ';
  var key = 'AIzaSyADb1NFtyu-2ruadimQ5Cys9ATpzRU2jyo';


  $.get( "https://www.googleapis.com/youtube/v3/playlistItems?part=contentDetails&part=snippet&part=id&part=status", {
    maxResults: 50,
    playlistId: playlistId,
    key: key
   } )
    .done(function( data ) {
      // console.log(data.items[0]);

      $(data.items).each(function(){
        var date = '<hr><div><em>Published: '+this.contentDetails.videoPublishedAt+'</em></div>';
        var title = '<h2>'+this.snippet.title+'</h2>';
        var description = '<p>'+this.snippet.description+'</p>';
        var link = '<div>https://www.youtube.com/watch?v='+this.snippet.resourceId.videoId+'</div>';
        var video = '<div class="video-responsive"><iframe width="560" height="315" src="https://www.youtube.com/embed/'+this.snippet.resourceId.videoId+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></div>';
        $('#youtubePlaylist').append('<div class="col-sm-4"><div class="video-card">'+video+title+description+date+'</div></div>');
      });

    });


      $('.youtube-video').each(function(){
        var videoId = $(this).data('id');
        var h = '<iframe width="560" height="315" src="https://www.youtube.com/embed/'+videoId+'" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>';
        $(this).html('<div class="video-responsive">'+h+'</div>');
      });


  // $.get('https://dan-drone.blogspot.com/feeds/posts/default')
  // .done(function (data) {
  //   console.log(data);
  // });

});
