---
layout: post
title:  "The First Flight"
date:   2020-07-29 13:05:00 -0400
categories:
---
The very first flight from 2017. It was a nail-biter because I didn't know what I was doing!

Below is the flight where, very boring to the experienced user, I thought I was going to crash. But, I was careful and, voila!!! I did it!


<div class="container-fluid">
  <div class="row">
    <div class="youtube-video col-sm-4 mb-3" data-id="cBuvGpo63cA"></div>
  </div>
</div>
