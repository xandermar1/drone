---
layout: page
title: My Drone
permalink: /drone/
---

My drone is a [GoPro Karma](https://gopro.com/en/us/yourkarma){:target="\_blank"}. I've used this drone for all of [my flights](/playlist) featured on this site. The biggest bummer is the Karma is not compatible with the HERO 8 - something I wish I knew before I bought the HERO 8. I'll be sure to be more cautious before buying GoPro items!




![GoPro Karma](https://static.turbosquid.com/Preview/2018/01/18__05_24_24/GoPro_Karma_00.jpg2327781C-C5CD-4035-A4BE-2BF5061649E9Large.jpg){:class="img-responsive"}
